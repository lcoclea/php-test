<?php

use App\Status;

if(!function_exists('table_class'))
{
    function table_class($statusId)
    {
        $class = "info";
        switch ($statusId)
        {
            case Status::ACTIVE:
                $class = "success";
                break;
            case Status::DELETED:
                $class = "danger";
                break;
        }
        return $class;
    }
}
