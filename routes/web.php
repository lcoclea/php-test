<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@list')->name('public.home');
Route::get('/search/{name}', 'HomeController@search')->name('public.pokemon.search');
Route::get('/pokemons/{pokemon}', 'HomeController@view')->name('public.pokemon.view');


Auth::routes();





