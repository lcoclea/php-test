<?php
Route::middleware('auth')->prefix('admin')->group(function () {
    Route::get('/dashboard', 'HomeController@index')->name('admin.dashboard');

    Route::get('/profile', 'ProfileController@index')->name('admin.profile');


    Route::prefix('pokemons')->group(function () {

        Route::get('/', 'PokemonController@list')->name('admin.pokemon.list');
        Route::post('/', 'PokemonController@list')->name('admin.pokemon.search');
        Route::post('/add', 'PokemonController@store')->name('admin.pokemon.store');

        Route::post('/{pokemon}/edit', 'PokemonController@update')->name('admin.pokemon.update');
        Route::get('/{pokemon}/delete', 'PokemonController@delete')->name('admin.pokemon.delete');
        Route::get('/{pokemon}/activate', 'PokemonController@activate')->name('admin.pokemon.activate');
    });

});
