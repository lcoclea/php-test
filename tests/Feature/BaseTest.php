<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

abstract class BaseTest extends TestCase
{
    use DatabaseMigrations;

    protected $faker;

    protected function setUp() :void
    {
        parent::setUp();

        $this->faker = app('Faker\Generator');

        $this->artisan('db:seed', ['--class' => 'DatabaseSeeder']);
    }

    protected function signIn()
    {
        $user = factory('App\User')->create();
        auth()->login($user);
    }
}
