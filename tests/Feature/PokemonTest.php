<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;

class PokemonTest extends BaseTest
{

    /** @test */
    public function a_unauthenticated_user_may_not_add_a_pokemon()
    {
        $attributes = [
            'name' => 'Ditto',
            'species' => 'Ditto',
            'height' => '20',
            'weight' => '40',
            'description' => $this->faker->text(),
        ];

        $response = $this->post(route('admin.pokemon.store'), $attributes);

        $this->assertEquals(302, $response->status());

    }


    /** @test */
    public function a_user_may_see_a_list_of_pokemons()
    {
        $this->signIn();

        $pokemon = factory('App\Pokemon')->create();

        $response = $this->get(route('admin.pokemon.list'));

        $response->assertSee($pokemon->name);
    }


    /** @test */
    public function a_authenticated_user_may_add_a_pokemon()
    {
        $this->signIn();

        $attributes = [
            'name' => 'Ditto',
            'species' => 'Ditto',
            'height' => '20',
            'weight' => '40',
            'description' => $this->faker->text(),
        ];

        $this->post(route('admin.pokemon.store'), array_merge($attributes, ['images' => [UploadedFile::fake()->image('test.png')]]));

        $this->assertDatabaseHas('pokemons', $attributes);

    }
}
