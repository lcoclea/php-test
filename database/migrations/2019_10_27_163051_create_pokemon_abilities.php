<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePokemonAbilities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pokemon_abilities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('pokemon_id');
            $table->string('name', 128);
            $table->unsignedInteger('status_id');
            $table->unsignedBigInteger('created_id')->nullable();
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();


            $table->foreign('pokemon_id', "fk::pokemon_abilities::pokemons")->references('id')->on('pokemons');
            $table->foreign('status_id', "fk::pokemon_abilities::statuses")->references('id')->on('statuses');
            $table->foreign('created_id', "fk::pokemon_abilities::created_user")->references('id')->on('users');
            $table->foreign('updated_id', "fk::pokemon_abilities::updated_user")->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pokemon_abilities');
    }
}
