<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePokemonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pokemons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 128)->unique();
            $table->string('species', 128);
            $table->integer('height');
            $table->integer('weight');
            $table->text('description');
            $table->unsignedInteger('status_id');
            $table->unsignedBigInteger('created_id')->nullable();
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();

            $table->foreign('status_id', "fk::pokemons::statuses")->references('id')->on('statuses');
            $table->foreign('created_id', "fk::pokemons::created_user")->references('id')->on('users');
            $table->foreign('updated_id', "fk::pokemons::updated_user")->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pokemons');
    }
}
