<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePokemonImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pokemon_images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pokemon_id');
            $table->string('path', 128);
            $table->unsignedInteger('status_id');
            $table->unsignedBigInteger('created_id')->nullable();
            $table->unsignedBigInteger('updated_id')->nullable();
            $table->timestamps();


            $table->foreign('pokemon_id', "fk::pokemon_images::pokemons")->references('id')->on('pokemons');
            $table->foreign('status_id', "fk::pokemon_images::statuses")->references('id')->on('statuses');
            $table->foreign('created_id', "fk::pokemon_images::created_user")->references('id')->on('users');
            $table->foreign('updated_id', "fk::pokemon_images::updated_user")->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pokemon_images');
    }
}
