<?php

use Illuminate\Database\Seeder;

class StatusSeader extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Status::create([
            'label' => 'Submitted'
        ]);

        \App\Status::create([
            'label' => 'Active'
        ]);

        \App\Status::create([
            'label' => 'Deleted'
        ]);
    }
}
