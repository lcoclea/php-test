# ukfast/pokédex

**This project will be based primarily on your ability to fulfill the task 
requirements. Any potential design skills are a bonus, but usability, 
performance and security will be taken into account.**

## Introduction
This project provides a starting point which will allow you to create your own 
web-based encyclopedia based on the popular franchise Pokémon - also known as 
a pokédex.

## Project Requirements
To get started, you'll need the following:

 - PHP
 - [Composer](https://getcomposer.org/)
 - git
 
 You are free to use whatever PHP packages and front-end libraries that you 
 wish.

## Task Requirements
To order to complete this challenge, you MUST create a pokédex with minimal 
functionality. Your solution MUST allow the user to browse the full list of 
pokémon in a convenient manner, as well as offer some form of search 
functionality. Your solution MUST also display basic information for a 
specific pokémon, including:

 - At least one image of the pokémon
 - Name
 - Species
 - Height and weight
 - Abilities
 
A RESTful API is available at [Pokéapi](https://pokeapi.co/) which will 
provide you with all the data that you will need. You do not need to create 
an account nor authenticate in order to consume the API, however please 
be aware that this API is rate-limited.
 
To get started, we've given you a skeleton folder structure. It is advised 
that you spend no more than two to three hours on this assignment.
 
## Submission
To submit your solution, please fork this repository and provide us a link 
to your finished version.

## Copyright
All trademarks as the property of their respective owners.

I just saw the last tickbox, pokemon abilities... not an excuse 
but it was probably the hours i worked on it

##Todos
- Add pokemon abilities 
- Backend search 
- Migrate to laravel translations
- Add more pokemon details from PokeApi
- More images to create a pokemon gallery

##Installation

Install the required packages. A new fresh instance of Laravel will be created
```shell script
composer install
```
Set up your environment 
```shell script
cp .env.example .env
```

& edit .env with your database credentials


Run the migrations and seeders

```shell script
php artisan migrate
```

```shell script
php artisan db:seed
```

Start a new testing server
```shell script
php artisan serve
```
This will create a temp server 127.0.0.1:8000 in witch this app javascript was compiled

*Please note that this project has some npm packages such as vue and other 
third parties libraries. If you choose to change the the app url you will need to have 
node.js and npm installed on your machine

After you can just run

```shell script
npm install && npm run dev
```
Screens:
![alt text](screens/add_pokemon.png "Screen 1")

![alt text](screens/edit_pokemon.png "Screen 2")

![alt text](screens/public_list.png "Screen 3")

![alt text](screens/public_search.png "Screen 4")

![alt text](screens/public_view.png "Screen 5")
