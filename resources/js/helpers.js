export default {
	asset: function (url) {
		if (url.charAt(0) == "/") url = url.substr(1);

		return "/" + url;
	},
};
