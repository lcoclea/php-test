import ZiggyRouter from 'ziggy';

class Router extends ZiggyRouter {
    constructor(name, params, absolute, ziggy) {
        super(name, params, absolute, ziggy);


        this.has = (name) => {
            return this.check(name);
        }
    }
}

export default Router;
