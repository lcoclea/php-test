import { Ziggy } from './routes';
import helpers from './helpers';
import Router from './router';
window.$ = window.jQuery = require('jquery');
window.Vue = require('vue');
import flash from './flash';
import Multiselect from 'vue-multiselect';
import VueSlideoutPanel from 'vue2-slideout-panel';

window.toastr = require('toastr');
window.toastr.options.closeButton = true;
window.toastr.options.progressBar = true;
window.toastr.options.timeOut = 6000; // How long the toast will display without user interaction
window.toastr.options.extendedTimeOut = 6000; // How long the toast will display after a user hovers over it
window.Flash = new flash();;

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

window.app = document.getElementById('vue');

window.vuebus = new Vue();
Vue.use(require('vue-cookies'));
Vue.use(VueSlideoutPanel);

Vue.mixin({
    methods: {
        ...helpers,
        route: function ( name, params, absolute ) {
            return new Router(name, params, absolute, Ziggy);
        },
    }
});

Vue.component('spinner', require('@app/vues/fields/spinner.vue').default);
Vue.component('csrf', require('@app/vues/fields/csrf.vue').default);
Vue.component('email', require('@app/vues/fields/email.vue').default);
Vue.component('username', require('@app/vues/fields/username.vue').default);
Vue.component('password', require('@app/vues/fields/password.vue').default);
Vue.component('add-form', require('@app/vues/components/add-form.vue').default);
Vue.component('view-form', require('@app/vues/components/view-form.vue').default);
Vue.component('search-pokemon', require('@app/vues/components/search-pokemon.vue').default);
Vue.component('multiselect', Multiselect);
