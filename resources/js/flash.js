
let Flash = function(){
    return {
        success(message, title = '')
        {
            this.show(message, title, 'success')
        },


        error(message, title = '')
        {
            this.show(message, title, 'error')
        },


        warning(message, title = '')
        {
            this.show(message, title, 'warning')
        },

        info(message, title = '')
        {
            this.show(message, title)
        },

        show(message, title = '', type = 'info') {
            if(message.hasOwnProperty('title'))
            {
                title = message.title;
            }

            if(message.hasOwnProperty('message'))
            {
                message = message.message;
            }

            if(message.hasOwnProperty('type'))
            {
                type = message.type;
            }

            window.toastr[type](message, title);
        },

        handle(props) {
            if(props.hasOwnProperty('infos') && props.infos.length > 0) {
                props.infos.forEach( (message) => {
                    this.show(message)
                })
            }

            if(props.hasOwnProperty('successes') && props.successes.length > 0) {
                props.successes.forEach( (message) => {
                    this.show(message, '', 'success')
                })
            }

            if(props.hasOwnProperty('warnings') && props.warnings.length > 0) {
                props.warnings.forEach( (message) => {
                    this.show(message, '', 'warning')
                })
            }

            if(props.hasOwnProperty('errors') && props.errors.length > 0) {
                props.errors.forEach( (message) => {
                    this.show(message, '', 'error')
                })
            }
        },
    }
}

export default Flash;
