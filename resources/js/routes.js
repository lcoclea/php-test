    var Ziggy = {
        namedRoutes: {"admin.dashboard":{"uri":"admin\/dashboard","methods":["GET","HEAD"],"domain":null},"admin.profile":{"uri":"admin\/profile","methods":["GET","HEAD"],"domain":null},"admin.pokemon.list":{"uri":"admin\/pokemons","methods":["GET","HEAD"],"domain":null},"admin.pokemon.search":{"uri":"admin\/pokemons","methods":["POST"],"domain":null},"admin.pokemon.store":{"uri":"admin\/pokemons\/add","methods":["POST"],"domain":null},"admin.pokemon.update":{"uri":"admin\/pokemons\/{pokemon}\/edit","methods":["POST"],"domain":null},"admin.pokemon.delete":{"uri":"admin\/pokemons\/{pokemon}\/delete","methods":["GET","HEAD"],"domain":null},"admin.pokemon.activate":{"uri":"admin\/pokemons\/{pokemon}\/activate","methods":["GET","HEAD"],"domain":null},"public.home":{"uri":"\/","methods":["GET","HEAD"],"domain":null},"public.pokemon.search":{"uri":"search\/{name}","methods":["GET","HEAD"],"domain":null},"public.pokemon.view":{"uri":"pokemons\/{pokemon}","methods":["GET","HEAD"],"domain":null},"login":{"uri":"login","methods":["GET","HEAD"],"domain":null},"logout":{"uri":"logout","methods":["POST"],"domain":null},"register":{"uri":"register","methods":["GET","HEAD"],"domain":null},"password.request":{"uri":"password\/reset","methods":["GET","HEAD"],"domain":null},"password.email":{"uri":"password\/email","methods":["POST"],"domain":null},"password.reset":{"uri":"password\/reset\/{token}","methods":["GET","HEAD"],"domain":null},"password.update":{"uri":"password\/reset","methods":["POST"],"domain":null},"password.confirm":{"uri":"password\/confirm","methods":["GET","HEAD"],"domain":null}},
        baseUrl: 'http://127.0.0.1:8000/',
        baseProtocol: 'http',
        baseDomain: '127.0.0.1',
        basePort: 8000,
        defaultParameters: []
    };

    if (typeof window.Ziggy !== 'undefined') {
        for (var name in window.Ziggy.namedRoutes) {
            Ziggy.namedRoutes[name] = window.Ziggy.namedRoutes[name];
        }
    }

    export {
        Ziggy
    }
