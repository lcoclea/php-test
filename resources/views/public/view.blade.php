@extends('layouts.frontend')

@section('title')
    {{config('app.name')}} - {{$pokemon->name}}
@endsection

@section('css')
@endsection

@section('content')
    <div class="container">

        <h2 class="text-primary"> {{$pokemon->name}} </h2>

        <div class="row">
            <div class="col-md-5">
                <img src="{{asset($pokemon->images->first()->path)}}" class="w-100 mb-3" alt="{{ $pokemon->name }}">


                <div class="row">
                    @foreach($pokemon->images as $image)
                        <div class="col-md-3">
                            <img src="{{ asset($image->path) }}" class="d-block" style="height: 120px" alt="...">
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-7">
                <div class="card mb-3">
                    <div class="card-body bg-primary text-white">
                        <label class="h2"><i class="fas fa-list"></i> Details: </label>
                        <div class="row">
                            <div class="col-md-6"> <h4>Height: {{ $pokemon->height }}</h4> </div>
                            <div class="col-md-6"> <h4>Weight: {{ $pokemon->weight }}</h4> </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-3">
                    <div class="card-body bg-success text-white">
                        <label class="h2"><i class="fab fa-react"></i> Abilities: </label>
                        <div class="row">
                            @foreach($pokemon->abilities as $ability)
                                <div class="col-md-6"> <h4>{{ $ability->name }}</h4> </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="card mb-3">
                    <div class="card-body">
                        {!! $pokemon->description !!}
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection

@section('scripts')
@endsection
