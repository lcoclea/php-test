@extends('layouts.frontend')

@section('title')
@endsection

@section('css')
@endsection

@section('content')
    <div class="container">

        <div class="row">
            <search-pokemon></search-pokemon>
        </div>


        @include('public.common.pagination', array('collections' => $pokemons))

        <div class="row">
            @foreach($pokemons as $pokemon)
                <div class="col-md-3 mb-3">
                    <div class="card">
                        <img src="{{asset($pokemon->images->first()->path)}}" class="card-img-top" alt="{{ $pokemon->name }}">
                        <div class="card-body">
                            <h5 class="card-title">{{ $pokemon->name }}</h5>
                            <a href="{{ route('public.pokemon.view', ['pokemon' => $pokemon])}}" class="btn btn-primary">View</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        @include('public.common.pagination', array('collections' => $pokemons))
    </div>
@endsection

@section('scripts')
@endsection
