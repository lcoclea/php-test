
<div class="d-block">
    <div class="row mb-2">
        <div class="col-md-3">
            <div class="" style="font-size: 18px;">
                @if(isset($results))
                    {{trans('pagination.total') . ': ' . $collections->total()}}
                @endif
            </div>
        </div>

        <div class="col-md-9">
            <nav class="float-right">
                {!! $collections->render() !!}
            </nav>
        </div>
    </div>
</div>

