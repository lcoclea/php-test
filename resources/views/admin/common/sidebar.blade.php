<nav id="sidebar">
    <ul class="list-unstyled">
        <div class="navbar-brand container-fluid">
            <img class="w-75 rounded" src="{{asset('img/logo.png')}}"/>
            <span class="ml-2 pt-5">{{__('Pokédex')}}</span>
        </div>

        <li data-toggle="tooltip" data-placement="right" title="Dashboard">
            <a href="{{route('admin.dashboard')}}"><i class="fas fa-tachometer-alt" ></i> <span>Dashboard</span></a>
        </li>

        <li>
            <a href="{{route('admin.pokemon.list')}}"><i class="fas fa-dragon"></i> <span>Pokémons</span></a>
        </li>
    </ul>
</nav>
