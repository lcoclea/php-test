<div class="text-grey" style="text-align: left">
    <i class="fa fa-history"></i> Last updated
    @if($record->updatedBy)
        by <img height="20px" src="{{asset($record->updatedBy->avatar)}}"> {{$record->updatedBy->name ?: ''}}:
    @endif
    {{$record->updated_at->diffForHumans()}}
</div>
