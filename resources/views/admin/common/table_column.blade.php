<div class="panel panel-table">
    <div class="panel panel-table">
        <div class="panel-heading">{{$title}}</div>
        <div class="panel-body">
            {{$slot}}
        </div>
    </div>
</div>