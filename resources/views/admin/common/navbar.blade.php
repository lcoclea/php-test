<nav id="app-navbar" class="navbar navbar-expand-md fixed-top bg-pokedex bg-white">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-content" aria-controls="navbar-content" aria-expanded="false" :aria-label="__('ui::app.toggle_nav')">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar-content">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto w-75 fs-50">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('public.home')}}">{{ __('Facing Site') }}</a>
                </li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <li v-else class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img height="24px" src="{{asset(auth()->user()->avatar)}}">
                        {{ auth()->user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu dropdown-menu-right mr-1 shadow" aria-labelledby="navbarDropdown">
                        <a href="#" class="dropdown-item" onclick="$('#logout-form').submit()"><i class="fa fa-sign-out-alt fa-fw"></i> {{ __('Logout') }}</a>
                        <form class="d-none" id="logout-form" method="POST" action="{{route('logout')}}">@csrf</form>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>



