@php

    $name = !empty($search_criteria['name']) ?: '';
    $species = !empty($search_criteria['species']) ?: '';
    $statusId = !empty($search_criteria['status_id']) ?: '';

@endphp

<form action="{{route('admin.pokemon.search')}}" method="POST" role=form class="mb-0">
    @csrf
    <div class="row">

        <div class="col-md-4">
            <div class="form-group">
                <label>Name:</label>
                <input class="form-control" name="name" value="{{$name}}">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label>Species:</label>
                <input class="form-control" name="species" value="{{$species}}">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label>Status:</label>
                {{--@include('admin.common.status', array('statusId'=>$statusId))--}}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 form-group">
            <div class="float-right">
                <button class="btn btn-dark"><i class="fa fa-search"></i> Search</button>
            </div>
        </div>
    </div>
</form>
