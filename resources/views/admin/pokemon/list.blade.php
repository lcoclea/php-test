@extends('layouts.backend')

@section('css')

@endsection
<link href="{{asset('css/responsive-tables.css') }}" rel="stylesheet">
@section('content')

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-2 pb-2 mb-2">
        <h2 class="text-normal"><i class='fas fa-dragon'></i> Pokemons</h2>
        <div class="btn-toolbar mb-2 mb-md-0">
            <add-form></add-form>
        </div>
    </div>


    <div class="card p-3 mb-3">
        <div class="row">
            <div class="col-md-6">
                <h2 class="text-normal"><i class="fa fa-filter"></i> Filters</h2>
            </div>
            <div class="col-md-6">
                <button class="btn btn-no-shadows btn-light float-right" type="button" data-toggle="collapse" data-target="#search_filters" aria-expanded="false" aria-controls="search_filters" data-placement="top" title="@lang('app.actions.toggle_filters')"><i class="fas fa-sliders-h"></i></button>
            </div>
        </div>
        <div class="collapse" id="search_filters">
            @include('admin.pokemon.search')
        </div>
    </div>

    @include('admin.common.pagination', array('collections' => $pokemons))

    @forelse($pokemons as $pokemon)
        <div class="card p-1 mb-2">
            <div class="table-div table-cols-5 row alert-{{ table_class($pokemon->status_id) }}">
                <div class="col-md-12">

                    <div class="table-number">
                        @component('admin.common.table_column', ['title' => "#"])
                            {{ $pokemon->id }}
                        @endcomponent
                    </div>
                    <div class="table-col-1">
                        @component('admin.common.table_column', ['title' => trans('pokemon.attributes.name')])
                            @if($pokemon->images->count())
                           <img src="{{ asset($pokemon->images->first()->path) }}" height="32px">
                            @endif
                            {{ $pokemon->name }}
                        @endcomponent
                    </div>
                    <div class="table-col-1">
                        @component('admin.common.table_column', ['title' => trans('pokemon.attributes.species')])
                            {{ $pokemon->species }}
                        @endcomponent
                    </div>
                    <div class="table-col-1">
                        @component('admin.common.table_column', ['title' => trans('pokemon.attributes.height')])
                            {{ $pokemon->height }}
                        @endcomponent
                    </div>
                    <div class="table-col-1">
                        @component('admin.common.table_column', ['title' => trans('pokemon.attributes.weight')])
                            {{ $pokemon->weight }}
                        @endcomponent
                    </div>
                    <div class="table-col-1">
                        @component('admin.common.table_column', ['title' => trans('pokemon.attributes.status')])
                            {!! $pokemon->status->label !!}
                        @endcomponent
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mb-2">
                    @include('admin.common.last_updated', ['record' => $pokemon])
                </div>
                <div class="col-md-6 mb-2">
                    @if($pokemon->status_id === \App\Status::ACTIVE)
                    <a href="{{route('admin.pokemon.delete', [$pokemon])}}" class="btn btn-danger float-right"><i class="fa fa-trash"></i> @lang('pokemon.actions.delete')</a>
                    @else
                        <a href="{{route('admin.pokemon.activate', [$pokemon])}}" class="btn btn-success float-right"><i class="fa fa-check"></i> @lang('pokemon.actions.activate')</a>
                    @endif
                    <view-form :data="{{json_encode($pokemon)}}"></view-form>
                </div>
            </div>
        </div>
    @empty
        @include('admin.common.no_data')
    @endforelse

    @include('admin.common.pagination', array('collections' => $pokemons))

@endsection
