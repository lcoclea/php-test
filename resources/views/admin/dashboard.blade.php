@extends('layouts.backend')

@section('content')

    <h2>
        <i class="fas fa-tachometer-alt"></i>
       Dashboard
    </h2>



    <div class="card">

        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            You are logged in!
        </div>
    </div>

@endsection
