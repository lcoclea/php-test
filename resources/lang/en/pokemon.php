<?php
/*
 * Just an example of my type of translations
 * i always do this type of translations
 * even for the smallest thing in my projects
 * in different files, of course
 * */

return [
    'attributes' => [
        'name' => "Name",
        'species' => "Species",
        'height' => "Height",
        'weight' => "Weight",
        'description' => "Description",
    ],

    ''=> [],

    'actions' => [
        'add' => 'Add new pokemon',
        'view' => 'View',
        'delete' => 'Delete',
        'activate' => 'Activate',
    ]

    //so on and so forth
];
