const mix = require('laravel-mix');
const WebpackShellPlugin = require('webpack-shell-plugin');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js/boot.js')
    .js('resources/js/setup.js', 'public/js/app.js')
    .sass('resources/sass/app.scss', 'public/css/bootstrap.css')
    .sass('resources/sass/vue-select.scss', 'public/css')
    .sass('resources/sass/toastr.scss', 'public/css')
    .sass('resources/sass/sidebar.scss', 'public/css')
    .sass('resources/sass/responsive-tables.scss', 'public/css')
    .webpackConfig({
        resolve: {
            alias: {
                '@app': path.resolve('resources'),
                'ziggy': path.resolve('vendor/tightenco/ziggy/dist/js/route.js'),
            },
        },
        plugins: [
            new WebpackShellPlugin({onBuildStart:[
                    'php artisan ziggy:generate --quiet resources/js/routes.js'
                ], onBuildEnd:[]}),
        ],
    }).extract()
    .version()
    .sourceMaps();

