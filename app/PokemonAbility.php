<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PokemonAbility extends Model
{
    //
    public function create(Pokemon $pokemon, string $ability)
    {
        $this->pokemon_id = $pokemon->id;
        $this->name = $ability;
        $this->status_id = Status::ACTIVE;

        $this->save();
    }
}
