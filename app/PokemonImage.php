<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class PokemonImage extends Model
{
    protected $fillable = [
        'pokemon_id', 'path', 'status_id'
    ];

    public function create(Pokemon $pokemon, UploadedFile $image)
    {
        $this->pokemon_id = $pokemon->id;
        $this->path = $image->store('/img/pokemons', ['disk' => 'pokemons']);
        $this->status_id = Status::ACTIVE;

        $this->save();
    }

    public function replace(UploadedFile $image)
    {
        $this->path = $image->store('/img/pokemons', ['disk' => 'pokemons']);
        $this->save();
    }
}
