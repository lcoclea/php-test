<?php

namespace App;

use App\Http\Requests\PokemonCreateRequest;
use App\Http\Requests\PokemonUpdateRequest;
use Illuminate\Support\Str;

class Pokemon extends Model
{
    protected $table = 'pokemons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'species', 'height', 'weight', 'status_id'
    ];

    protected $with = ['status', 'images', 'abilities'];

    public function create(PokemonCreateRequest $request)
    {
        $this->name = Str::title($request->name);
        $this->species = Str::title($request->species);
        $this->height = $request->height;
        $this->weight = $request->weight;
        $this->description = $request->description;
        $this->status_id = Status::ACTIVE;

        $this->save();
    }

    public function edit(PokemonUpdateRequest $request)
    {
        $this->description = $request->description;
        $this->save();
    }

    public function deactivate()
    {
        $this->status_id = Status::DELETED;

        $this->save();
    }

    public function activate()
    {
        $this->status_id = Status::ACTIVE;

        $this->save();
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function images()
    {
        return $this->hasMany(PokemonImage::class);
    }

    public function abilities()
    {
        return $this->hasMany(PokemonAbility::class);
    }

    public function addImage($image)
    {
        $pokemonImage = new PokemonImage();
        $pokemonImage->create($this, $image);

        return $pokemonImage;
    }

    public function addAbility($name)
    {
        $ability = new PokemonAbility();
        $ability->create($this, $name);

        return $ability;
    }

    public function replaceImage(int $key, $image)
    {
        $images = $this->images;

        if($pokemonImage = $images->get($key))
        {
            $pokemonImage->replace($image);
        }

    }
}
