<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PokemonCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:pokemons|string|max:128',
            'species' => 'required|string|max:128',
            'height' => 'required|integer',
            'weight' => 'required|integer',
            'description' => 'required|string|max:1000',
            'images' => 'required|array',
            'abilities' => 'required|array',
        ];
    }
}
