<?php

namespace App\Http\Controllers;

use App\Pokemon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function list()
    {
        return view('public.welcome')
            ->with('pokemons', Pokemon::paginate(24));
    }

    public function search($name)
    {
        $pokemons = Pokemon::where('name', "like", "%$name%")->take(25)->get();

        return response()->json(compact('pokemons'));
    }

    public function view(Pokemon $pokemon)
    {
        return view('public.view')
            ->with('pokemon', $pokemon);
    }
}
