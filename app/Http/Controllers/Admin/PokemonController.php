<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PokemonCreateRequest;
use App\Http\Requests\PokemonUpdateRequest;
use App\Pokemon;

class PokemonController extends Controller
{
    public function list()
    {
        return view('admin.pokemon.list')
            ->with('pokemons', Pokemon::latest()->paginate(25));
    }

    public function store(PokemonCreateRequest $request, Pokemon $pokemon)
    {
        $pokemon->create($request);

        foreach ($request->images as $image)
        {
            $pokemon->addImage($image);
        }

        foreach ($request->abilities as $ability)
        {
            $pokemon->addAbility($ability);
        }

        if($request->expectsJson())
        {
            return response()->json([
                'status'=>'Success',
                'message'=>'The pokemon was added'
            ]);
        }

        return route('admin.pokemon.list')->with('successes', ['The pokemon was added']);
    }


    public function update(PokemonUpdateRequest $request, Pokemon $pokemon)
    {
        $pokemon->edit($request);

        if($request->has('images'))
        {
            $images = $pokemon->images;

            foreach ($request->images as $key => $image)
            {
                if($pokemonImage = $images->get($key))
                {
                    $pokemonImage->replace($image);
                }
                else
                {
                    $pokemon->addImage($image);
                }
            }
        }

        if($request->expectsJson())
        {
            return response()->json([
                'status'=>'Success',
                'message'=>'The pokemon was edited'
            ]);
        }

        return route('admin.pokemon.list')->with('successes', ['The pokemon was edited']);
    }

    public function delete(Pokemon $pokemon)
    {
        $pokemon->deactivate();

        return redirect(route('admin.pokemon.list'))->with('successes', ['The pokemon was deleted']);
    }

    public function activate(Pokemon $pokemon)
    {
        $pokemon->activate();

        return redirect(route('admin.pokemon.list'))->with('successes', ['The pokemon was activated']);
    }
}
