<?php

namespace App;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){
            if(!$model->created_id)
            {
                $model->created_id = auth()->check() ? auth()->user()->id : null;
            }

            if(!$model->updated_id)
            {
                $model->updated_id = auth()->check() ? auth()->user()->id : null;
            }
        });

        self::updating(function($model){
            $model->updated_id = auth()->check() ? auth()->user()->id : null;
        });
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_id');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_id');
    }
}
