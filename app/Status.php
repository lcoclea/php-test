<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    //
    const SUBMITTED = 1;
    const ACTIVE = 2;
    const DELETED = 3;
}
